import React, { Component } from 'react';
import arrowLeftImage from '../img/arrowLeft.png';
import book from '../img/book.png';
import add from '../img/add.png';
import all from '../img/all.png';
import active from '../img/active.png';
import inactive from '../img/inactive.png';
import order from '../img/order.png';
import user from '../img/user.png';
import settings from '../img/setting.png';
import home from '../img/home.png';
import logout from '../img/logout.png';
import update from '../img/update.png';

import { Link } from 'react-router-dom';

import AddProduct from '../fragment/AddProduct'
import UpdateProduct from '../fragment/UpdateProduct'
import AllProduct from '../fragment/AllProduct'
import ActiveProduct from '../fragment/ActiveProduct'
import InactiveProduct from '../fragment/InactiveProduct'
import Orders from '../fragment/Orders'
import Users from '../fragment/Users'
import Settings from '../fragment/Settings'

// const AdminDashboard = () => {
//   const [open, setOpen] = useState(true);
//   const [selectedMenu, setSelectedMenu] = useState(0);
//   const [hideText, setHideText] = useState(false);

//   const handleMenuHover = (menuIndex) => {
//     if (selectedMenu !== menuIndex) {
//       setSelectedMenu(menuIndex);
//     }
//   };

  class AdminDashboard extends Component {
    constructor(props) {
      super(props);
      this.state = {
        open: true,
        selectedMenu: 0,
        hideText: false,
      }
    }

    handleMenuHover(menuIndex) {
      if(this.state.selectedMenu !== menuIndex) {
        // setSelectedMenu(menuIndex);
        this.setState({
          selectedMenu: menuIndex
        });
      }
    }

    handleOpenCloseBtn(){
      if(this.state.open === true) {
        this.setState({
          open: false
        })
      } else {
        this.setState({
          open: true
        })
      }
    }

    render() {
      return (
        <div className="flex">
          <div className={`${this.state.open ? "w-[340px]" : "w-[120px]"} duration-300 h-screen p-3 pt-4 bg-black relative`}>
            <img
              src={arrowLeftImage}
              className={`w-8 h-8 absolute cursor-pointer rounded-full -right-4 top-[78px] transform -translate-y-1/2 border-[3px] border-black ${!this.state.open && "rotate-180"}`}
              alt="Arrow left"
              onClick={() => this.handleOpenCloseBtn()}
            />
            <div className="flex gap-x-5 mt-4 items-center border-b pb-4 border-gray-700">
              <img
                src={book}
                className={`cursor-pointer duration-500 w-12 h-12 ml-[20px]`}
              />
              <h1
                className={`text-white origin-left font-medium text-xl duration-300 mt-1 ${!this.state.open && 'scale-0'}`}>Admin <br/> Dashboard
              </h1>
            </div>
            <div>
              {/*Add Product*/}
              <div className="pt-[24px] pl-1 pb-[5px] -ml-[5px]">
                <div
                  className={`text-black text-sm flex items-center gap-x-5 cursor-pointer p-3 hover:bg-gray-500 rounded-md ${this.state.selectedMenu === 1 ? "bg-gray-500" : ""}`}
                  onClick={() => this.handleMenuHover(1)}
                >
                  <img src={add} className="w-8 h-8 ml-3" />
                  <span className={`ml-2 mr-4 text-gray-100 font-medium text-[15px] ${!this.state.open && 'hidden'} origin-left duration-200`}>Create
                  </span>
                </div>
               </div>

              {/*All Product*/}
              <div className="pl-1 pb-[5px] -ml-[5px]">
                <div
                  className={`text-black text-sm flex items-center gap-x-5 cursor-pointer p-3 hover:bg-gray-500 rounded-md ${this.state.selectedMenu === 2 ? "bg-gray-500" : ""}`}
                  onClick={() => this.handleMenuHover(2)}
                >
                  <img src={all} className="w-8 h-8 ml-3" />
                  <span className={`ml-2 mr-4 text-gray-100 font-medium text-[15px] ${!this.state.open && 'hidden'} origin-left duration-200`}>All
            </span>
          </div>
           </div>

           {/*Active Product*/}
              <div className="pl-1 pb-[5px] -ml-[5px]">
                <div
              className={`text-black text-sm flex items-center gap-x-5 cursor-pointer p-3 hover:bg-gray-500 rounded-md ${this.state.selectedMenu === 3 ? "bg-gray-500" : ""}`}
              onClick={() => this.handleMenuHover(3)}
            >
              <img src={active} className="w-8 h-8 ml-3" />
              <span className={`ml-2 mr-4 text-gray-100 font-medium text-[15px] ${!this.state.open && 'hidden'} origin-left duration-200`}>Active
              </span>
            </div>
          </div>

          {/*Inactive Product*/}
          <div className="pl-1 pb-[50px] -ml-[5px]">
            <div
              className={`text-black text-sm flex items-center gap-x-5 cursor-pointer p-3 hover:bg-gray-500 rounded-md ${this.state.selectedMenu === 4 ? "bg-gray-500" : ""}`}
              onClick={() => this.handleMenuHover(4)}
            >
              <img src={inactive} className="w-8 h-8 ml-3" />
              <span className={`ml-2 mr-4 text-gray-100 font-medium text-[15px] ${!this.state.open && 'hidden'} origin-left duration-200`}>Inactive
              </span>
            </div>
          </div>

          {/*Orders*/}
          <div className="pl-1 pb-[5px] -ml-[5px]">
            <div
              className={`text-black text-sm flex items-center gap-x-5 cursor-pointer p-3 hover:bg-gray-500 rounded-md ${this.state.selectedMenu === 5 ? "bg-gray-500" : ""}`}
              onClick={() => this.handleMenuHover(5)}
            >
              <img src={order} className="w-8 h-8 ml-3" />
              <span className={`ml-2 mr-4 text-gray-100 font-medium text-[15px] ${!this.state.open && 'hidden'} origin-left duration-200`}>Orders
              </span>
            </div>
          </div>

          {/*Users*/}
          <div className="pl-1 pb-[50px] -ml-[5px]">
            <div
              className={`text-black text-sm flex items-center gap-x-5 cursor-pointer p-3 hover:bg-gray-500 rounded-md ${this.state.selectedMenu === 6 ? "bg-gray-500" : ""}`}
              onClick={() => this.handleMenuHover(6)}
            >
              <img src={user} className="w-8 h-8 ml-3" />
              <span className={`ml-2 mr-4 text-gray-100 font-medium text-[15px] ${!this.state.open && 'hidden'} origin-left duration-200`}>Users
              </span>
            </div>
          </div>

          {/*Home*/}
          <div className="pl-1 pb-[5px] -ml-[5px]">
            <Link to={`/home?userId=${user.id}`} className={`text-decoration-none text-black text-sm flex items-center gap-x-5 cursor-pointer p-3 hover:bg-gray-500 rounded-md ${this.state.selectedMenu === 7 ? "bg-gray-500" : ""}`} onClick={() => this.handleMenuHover(7)}>
              <img src={home} className="w-8 h-8 ml-3" />
              <span className={`ml-2 mr-4 text-gray-100 font-medium text-[15px] ${!this.state.open && 'hidden'} origin-left duration-200`}>Home</span>
            </Link>
          </div>

        {/*Settings*/}
          <div className="pl-1 pb-[5px] -ml-[5px]">
            <div
              className={`text-black text-sm flex items-center gap-x-5 cursor-pointer p-3 hover:bg-gray-500 rounded-md ${this.state.selectedMenu === 8 ? "bg-gray-500" : ""}`}
              onClick={() => this.handleMenuHover(8)}
        >
          <img src={settings} className="w-8 h-8 ml-3" />
          <span className={`ml-2 mr-4 text-gray-100 font-medium text-[15px] ${!this.state.open && 'hidden'} origin-left duration-200`}>Settings
          </span>
        </div>
        </div>

        {/*Logout*/}
        <div className="pl-1 pb-[5px] -ml-[5px]">
          <a href="/logout" className={`text-decoration-none text-black text-sm flex items-center gap-x-5 cursor-pointer p-3 hover:bg-gray-500 rounded-md ${this.state.selectedMenu === 9 ? "bg-gray-500" : ""}`} onClick={() => this.handleMenuHover(9)}>
            <img src={logout} className="w-8 h-8 ml-3" />
            <span className={`ml-2 mr-4 text-gray-100 font-medium text-[15px] ${!this.state.open && 'hidden'} origin-left duration-200`}>Logout</span>
          </a>
        </div>
      </div>
    </div>
    <div className="bg-gray-100 w-full h-screen">
    {(() => {
      switch (this.state.selectedMenu) {
        case 1:
          return <AddProduct />;
        case 2:
          return <AllProduct />;
        case 3:
          return <ActiveProduct />;
        case 4:
          return <InactiveProduct />;
        case 5:
          return <Orders />;
        case 6:
          return <Users />;
        default:
          return <Settings />;
        }
      })()}
    </div>
  </div>
  );
}
}

export default AdminDashboard;  