import React, { useContext } from 'react';
import { useParams } from 'react-router-dom';
import { CartContext } from '../context/CartContext';
import { ProductContext } from '../context/ProductContext';
import { useNavigate } from 'react-router-dom';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const ProductDetails = () => {
	const { _id } = useParams();
	const { products } = useContext(ProductContext);
	const { addToCart } = useContext(CartContext);

	const product = products.find(item => item._id === _id);

	const navigate = useNavigate();
	 const handleBack = () => {
	    navigate(-1);
	  }

	if (!product) {
		return <section className="h-screen flex justify-center items-center">Loading...</section>;	
	}

	const { name, price, description, image, quantity } = product;

	// <div className="min-h-screen flex items-center justify-center bg-gray-100">
	// 	  <div className="bg-white w-[500px] px-8 py-10 rounded-md shadow-md shadow-lg">

	  const handleAddToCart = (product, id) => {
	  const token = localStorage.getItem('userId');
	  if (token) {
	    addToCart(product, id);
	    toast.success('Successfully added to cart!', {
	      position: toast.POSITION.TOP_RIGHT,
	      autoClose: 1000,
	      hideProgressBar: false,
	      closeButton: false
	    });
	  } else {
	    toast.error('Please log in to add to cart', {
	      position: toast.POSITION.TOP_RIGHT,
	      autoClose: 3000,
	      hideProgressBar: false,
	      closeButton: false
	    });
	  }
	}

	return (
		<section className="bg-gray-100 pt-[60px] pb-[40px] lg:py-32 h-screen flex items-center">
		<ToastContainer />
			<div className="bg-white pt-[70px] pb-[70px] rounded-md shadow-md shadow-lg container mx-auto">
				{/*image & text wrapper*/}
				<div className="flex flex-col lg:flex-row items-center">
					{/*image*/}
					<div className="flex flex-1 justify-center items-center mb-8 lg:mb-0">
						<img className="max-w-[150px] lg:max-w-sm shadow-md shadow-lg" src={image} alt="" />
					</div>
					{/*text*/}
					<div className="flex-1 lg:text-left">
						<h1 className="text-[26px] font-medium mb-2 mx-auto lg:mx-0">{name}</h1>
						<div className="text-xl text-red-500 font-medium mb-2">$ {price}
						</div>
						<p className="mb-8 mr-[90px]">{description}</p>
						<button onClick={handleBack} className="bg-gray-400 py-3 rounded-md px-6 text-white">
						Continue Shopping
						</button>
						<button onClick={() => handleAddToCart(product,product._id)} className="ml-5 bg-black py-3 rounded-md px-6 text-white">Add to cart
						</button>
					</div>
				</div>
			</div>
		</section>
	);
};

export default ProductDetails;