import AppNavbar from './components/AppNavbar';
import Sidebar from './components/Sidebar';
import Header from './components/Header';
import Home from './pages/Home'
import Login from './pages/Login'
import AdminDashboard from './pages/AdminDashboard'
import Logout from './pages/Logout'
import Error from './pages/Error'
import Register from './pages/Register'
import ProductDetails from './pages/ProductDetails'
import LandingPage from './pages/LandingPage'
import './App.css';

import { useState, useEffect } from 'react';

import { UserProvider } from './UserContext';

import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

function App() {

  const [ user, setUser ] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/${user.id}/details`)
    .then(res => res.json())
    .then(data => {
      console.log(data)
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [user.id]);

  return (
    <div className="overflow-hidden">
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <Sidebar />
        <AppNavbar />
          <Routes>
            <Route path="/" element={<LandingPage />} />
            <Route path="/home" element={<Home />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/adminDashboard" element={<AdminDashboard />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/products/:_id" element={<ProductDetails />} />
            <Route path="*" element={<Error/>} />
          </Routes>
      </Router>
    </UserProvider>
    </div>
  );
}

export default App;

