import React, { createContext, useState, useEffect } from 'react';
import Loading from '../components/Loading';

export const ProductContext = createContext();

const ProductProvider = ({ children }) => {

  const [products, setProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/active`)
      .then((res) => res.json())
      .then((data) => {
        setProducts(
          data.map((product) => {
            return { ...product, quantity: 0 };
          })
        );
        setIsLoading(false);
      })
      .catch((error) => console.error(error));
  }, []);

  return (
    (isLoading) ?
        <Loading />
       : 
        <ProductContext.Provider value={{ products }}>
          {children}
        </ProductContext.Provider>
  );
};

export default ProductProvider;