import React, { createContext, useState, useEffect } from 'react';

export const CartContext = createContext();

const CartProvider = ({children}) => {

    const [ cart, setCart ] = useState([]);
    const [ itemAmount, setItemAmount ] = useState(0);
    const [ total, setTotal ] = useState(0);

    useEffect(() => {
        const total = cart.reduce((accumulator, currentItem) => {
            return accumulator + currentItem.price * currentItem.amount;
        }, 0);
        setTotal(total);
    });

    useEffect(() => {
        if (cart) {
            const amount = cart.reduce((accumulator, currentItem) => {
                return accumulator + currentItem.amount;
            }, 0);
            setItemAmount(amount);
        }
    }, [cart])

    const addToCart = (product, _id) => {
        const newItem = { ...product, amount: 1 };

        const cartItem = cart.find(item => {
            return item._id === _id;
        });

        if (cartItem) {
            const newCart = [...cart].map(item => {
                if(item._id === _id) {
                    return {...item, amount: cartItem.amount + 1 };
                } else {
                    return item;
                }
            });
            setCart(newCart);
        } else {
            setCart([...cart, newItem]);
        }
    };

    const removeFromCart = (_id) => {
        const newCart = cart.filter(item => {
            return item._id !== _id;
        })
        setCart(newCart);
    };

    const clearCart = () => {
        setCart([]);
    };

    const increaseAmount = (_id) => {
        const cartItem = cart.find(item => item._id === _id);
        addToCart(cartItem, _id);
    };

    const decreaseAmount = (_id) => {
        const cartItem = cart.find(item => item._id === _id);
        if (cartItem) {
            if (cartItem.amount > 1) {
                const newCart = cart.map(item => {
                    if (item._id === _id) {
                        return {...item, amount: cartItem.amount - 1}
                    } else {
                        return item;
                    }
                });
                setCart(newCart);
            } 
        }
    };

    return ( 
        <CartContext.Provider value={{ 
            cart, 
            addToCart, 
            removeFromCart, 
            clearCart, 
            increaseAmount, 
            decreaseAmount,
            itemAmount,
            total,
        }}>{children}
        </CartContext.Provider>
    );
};

export default CartProvider;

