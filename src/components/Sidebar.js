import React, { useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { IoMdArrowForward } from 'react-icons/io';
import { FiTrash2 } from 'react-icons/fi';
import CartItem from '../components/CartItem';
import { SidebarContext } from '../context/SidebarContext';
import { CartContext } from '../context/CartContext';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { loadStripe } from '@stripe/stripe-js';

const Sidebar = () => {


  const { isOpen, handleClose } = useContext(SidebarContext);
  const { cart, clearCart, total, itemAmount } = useContext(CartContext);

  const { user, setUser } = useContext(UserContext);
  const [userId, setUserId] = useState("");
  const [products, setProducts] = useState("");
  const [price, setPrice] = useState("");
  const [totalAmount, setTotalAmount] = useState("");

  const handleClearCart = () => {
    if (cart.length === 0) {
      alert('Cart is empty! There are no items in your cart to clear.');
      return;
    }

    Swal.fire({
      title: 'Are you sure?',
      text: 'Are you sure you want to clear your cart?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, clear it!',
      cancelButtonText: 'No, cancel',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        clearCart();
        Swal.fire(
          'Cart Cleared!',
          'Your cart has been cleared.',
          'success'
        );
      }
    });
  };

  const handleCheckout = () => {
    if (cart.length === 0) {
      alert('Cart is empty! Please add some items to the cart.');
      return;
    }

    console.log(user);
    console.log(cart);
    let products = [];
    for (let i = 0; i < cart.length; i++) {
      console.log(cart[i]._id)
      let product = { 
        productId: cart[i]._id,
        name: cart[i].name,
        price: cart[i].price,
        quantity: cart[i].amount,
        subtotal: cart[i].price * cart[i].amount,
      }
      products.push(product);
    }

    const userId = user.id;
    console.log(userId)
    console.log(user.id)

    Swal.fire({
      title: 'Confirm Checkout',
      width: 800,
      html: `
        Are you sure you want to checkout with the following items?
        <ul style="list-style-type: none; padding: 0; margin-top: 20px;">
          ${products.map(product => `
            <li>
              <div style="display: flex; justify-content: space-between;">
                <span>${product.name} x ${product.quantity}</span>
                <span>$${product.subtotal.toFixed(2)}</span>
              </div>
            </li>
          `).join('')}
        </ul>
        <div style="display: flex; justify-content: space-between; align-items: center; margin-top: 10px; font-weight: bold; border-top: 1px solid #aaa;">
          <span>Total:</span>
          <span style="text-align: right; margin-top: 10px;">$${total.toFixed(2)}</span>
        </div>
      `,
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Confirm',
      cancelButtonText: 'Cancel',
    }).then((result) => {
      if (result.isConfirmed) {
        fetch(`${process.env.REACT_APP_API_URL}/order/checkout`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            userId: userId,
            products: products,
            price: price,
            totalAmount: total,
          })
        })
        .then(res => res.json())
        .then(data => {
          console.log(data)
          clearCart();
          Swal.fire(
            'Checkout Confirmed!',
            'Thank you for your purchase.',
            'success'
          );
        })
        .catch(error => {
          console.error('An error occurred during checkout:', error);
          Swal.fire(
            'Error Occurred!',
            'An error occurred during checkout. Please try again later.',
            'error'
          );
          console.error(error);
        });
      }
    });
  }

  return (
    <div
    className={`${isOpen ? 'right-0' : '-right-full'} w-full bg-white fixed top-0 h-full shadow-2xl md:w-[35vw] xl:max-w-[30vw] transition-all duration-300 z-20 px-4 lg:px-[35px] position:fixed bottom-0 z-10`}>
      <div className="flex items-center justify-between py-6 border-b">
        <div className="uppercase text-sm font-semibold">Shopping Cart ( {itemAmount} )</div>
        <div onClick={handleClose} className="cursor-pointer w-8 h-8 flex justify-center items-center">
          <IoMdArrowForward className="text-2xl" />
        </div>
      </div>
      <div className="flex flex-col gap-y-2 h-[620px] lg:h-[620px] overflow-y-auto overflow-x-hidden border-b">
        {cart.map(item => {
          return <CartItem item={item} key={item._id} />;
        })}
      </div>
      <div className="flex flex-col gap-y-3 py-4 mt-2">
        <div className="flex justify-between items-center">
          {/*Total: */}
          <div className="uppercase font-semibold mb-3">
            <span className="text-[17px]">Total: </span>
          </div>
          {/*(total)*/}
          <div className="uppercase font-semibold mb-3 text-right text-[17px]">
            <span className="">$ {parseFloat(total).toFixed(2)}</span>
          </div>
        </div>
        <button onClick={handleCheckout} className="bg-black flex p-3 justify-center items-center text-white w-full font-medium text-decoration-none cursor-pointer rounded-md">Checkout
        </button>
        <button onClick={handleClearCart} className="bg-gray-400 flex p-3 justify-center items-center text-white w-full font-medium text-decoration-none cursor-pointer rounded-md">Clear Cart
        </button>
      </div>
    </div>
  );
};

export default Sidebar;
