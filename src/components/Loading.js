import Spinner from 'react-bootstrap/Spinner';

function Loading() {
  return (
    <div style={{
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      height: '100vh'
    }}>
      <Spinner animation="border" variant="secondary" />
    </div>
  );
}

export default Loading;
