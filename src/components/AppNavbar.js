import React, { useEffect, useState, useContext } from 'react';
import { Link, NavLink, useLocation } from 'react-router-dom';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { BsCart } from 'react-icons/bs';
import { BsFillPersonFill } from 'react-icons/bs';
import { BsPersonFillGear } from 'react-icons/bs';
import UserContext from '../UserContext';

import book from '../img/book.png';

import { SidebarContext } from '../context/SidebarContext';
import { CartContext } from '../context/CartContext';


export default function AppNavbar() {
  const { user, setUser } = useContext(UserContext);
  const { isOpen, setIsOpen } = useContext(SidebarContext);
  const { itemAmount } = useContext(CartContext);

  const location = useLocation();
  const shouldShowNavbar = !location.pathname.includes('/adminDashboard');

  const [scrolled, setScrolled] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      const isScrolled = window.scrollY > 0;
      if (isScrolled !== scrolled) {
        setScrolled(isScrolled);
      }
    };

    document.addEventListener('scroll', handleScroll);

    return () => {
      document.removeEventListener('scroll', handleScroll);
    };
  }, [scrolled]);

  const navbarClassNames = scrolled
    ? 'navbar navbar-light bg-white position-fixed shadow-md shadow-lg w-full'
    : 'navbar navbar-light position-fixed w-full';

  const token = localStorage.getItem('userId');
  const brandLink = token !== null || user.id !== null ? '/home' : '/';

  console.log(user)

  return shouldShowNavbar ? (
    <div className="fixed z-10">
      <Navbar className={navbarClassNames} expand="lg">
        <Navbar.Brand className="ml-5" to={brandLink}>
          <a className="text-decoration-none flex items-center text-black text-[24px] ml-5" href="/">
            <img src={book} width="40" height="40" className="inline-block align-top mr-2" alt="" />
            <span className="text-decoration-none">Book Haven</span>
          </a>
        </Navbar.Brand>

        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse className="mr-5" id="basic-navbar-nav">
         <Nav className="ml-auto">
           {user.id !== null || token !== null ? (
             <>
               {user.isAdmin ? (
                 <Nav.Link
                   className="text-black"
                   as={NavLink}
                   to="/adminDashboard"
                 >
                   <BsPersonFillGear className="text-xl text-black d-inline-block align-top mr-1" />
                   Admin Dashboard
                 </Nav.Link>
               ) : null}
               <Nav.Link className="text-black" as={NavLink} to="/logout">
                 <BsFillPersonFill className="text-xl text-black d-inline-block align-top mr-1" />
                 Logout
               </Nav.Link>
             </>
           ) : (
             <>
               <Nav.Link
                 className="border border-0 bg-black bg-opacity-10 px-3 rounded-md text-black mr-2 ml-[1350px]"
                 as={NavLink}
                 to="/login"
               >
                 <BsFillPersonFill className="text-xl text-black d-inline-block align-top mr-1" />
                 Login/Register
               </Nav.Link>
               <Nav.Link className="text-black" as={NavLink} to="/register"></Nav.Link>
             </>
           )}

         </Nav>

          {/*Sidebar*/}
        <div className="ml-5 mr-5">
          {location.pathname !== '/' && location.pathname !== '/login' && location.pathname !== '/register' && (
            <div
              onClick={() => setIsOpen(!isOpen)}
              className="cursor-pointer flex relative mr-5 max-w-[50px]"
            >
              <BsCart className="text-xl text-black" />
              <div className="bg-red-500 absolute -right-2 -bottom-2 text-[12px] w-[18px] h-[18px] text-white rounded-full flex justify-center items-center">
                {itemAmount}
              </div>
            </div>
          )}
        </div>
      </Navbar.Collapse>
    </Navbar>
    </div>
  ) : null;
}