import React, { Component } from 'react';
import Loading from '../components/Loading';
import Swal from 'sweetalert2';

import { BsTrash } from 'react-icons/bs';

class Orders extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orders: [],
      activeOrders: [],
    };
  }

  componentDidMount() {
    fetch(`${process.env.REACT_APP_API_URL}/order/all`)
      .then(res => res.json())
      .then(data => {
        this.setState({ orders: data })
        console.log(data)
      })
  }

  formatDate = (dateString) => {
    const date = new Date(dateString);
    const formattedDate = `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()} ${date.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })}`;
    return formattedDate;
  }

  toggleOrderDetails = (orderId) => {
    this.setState(prevState => {
      const activeOrders = [...prevState.activeOrders];
      const orderIndex = activeOrders.indexOf(orderId);
      if (orderIndex >= 0) {
        activeOrders.splice(orderIndex, 1);
      } else {
        activeOrders.push(orderId);
      }
      return { activeOrders };
    });
  }

  handleDeleteClick(_id) {
    Swal.fire({
      title: 'Are you sure you want to delete this order?',
      text: 'You will not be able to recover this order after deletion!',
      iconColor: '#d1495b',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d1495b',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        const productId = _id;
        fetch(`${process.env.REACT_APP_API_URL}/order/${_id}/delete`, {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json'
          },
        })
        .then(res => res.json())
        .then(data => {
          this.componentDidMount();
          console.log(data);
          Swal.fire(
            'Deleted!',
            'The order has been deleted.',
            'success'
          )
        })
        .catch(error => console.error(error));
        console.log('delete order with id:', _id);
      }
    })
  }

  render() {
    const { orders, activeOrders } = this.state;

    return (
      <div className="p-5 mt-[12px]">
        <h1 className="text-3xl font-bold mb-[45px]">Orders</h1>
        <div className="border-b border-gray-300 mb-8"></div>
        <div className="bg-white p-10 rounded-md shadow-md mx-auto overflow-auto" style={{ maxHeight: 'calc(100vh - 180px)' }}>
          {orders.map((order) => (
            <div key={order._id} className="mb-3">
              <div className="py-3 cursor-pointer bg-gray-100 bg-opacity-80 text-black rounded-md px-[12px] py-1" onClick={() => this.toggleOrderDetails(order._id)}>
                <div className="flex justify-between">
                  <div className="flex items-center ml-[30px]" style={{ minWidth: '150px' }}>
                    <span className="text-md font-medium">Order ID:</span>
                    <span className="text-md ml-2 tracking-wider" style={{ minWidth: '250px' }}>{order._id}</span>
                  </div>
                  <div className="flex items-center">
                    <span className="text-md font-medium">User ID:</span>
                    <span className="text-md ml-2 tracking-wider">{order.userId}</span>
                  </div>
                  <div className="flex items-center">
                    <span className="text-md font-medium">Status:</span>
                    <span className="text-md ml-2 tracking-wider">Pending</span>
                  </div>
                  <div className="flex items-center">
                    <span className="text-md font-medium">Purchased On:</span>
                    <span className="text-md ml-2">{this.formatDate(order.purchasedOn)}</span>
                  </div>
                  <div className="flex items-center mr-[30px]">
                    <button
                      className="bg-[#d1495b] hover:bg-red-600 text-white font-bold py-2 px-4 rounded mb-1 w-[69px]"
                      onClick={() => this.handleDeleteClick(order._id)}>
                      <BsTrash className="text-xl text-white align-center justify-center" />
                    </button>
                  </div>
                </div>
              </div>

              <div className="flex justify-between mb-2">
              </div>
              {activeOrders.includes(order._id) && (
                <div className="bg-gray-100 bg-opacity-20 px-10 py-6 rounded-md shadow-md my-4">
                  <div className="mb-2 mt-2">
                    {order.products.map((product) => (
                      <div key={product._id} className="flex justify-between">
                        <span className="text-md">( x{product.quantity} ) {product.name}</span>
                        <span className="text-md">{product.price}</span>
                      </div>
                    ))}
                    <div className="border-b mt-3">
                    </div>
                  </div>
                  <div className="flex justify-between mb-2">
                    <span className="text-md font-medium">Total Amount:</span>
                    <span className="text-md font-medium">${order.totalAmount.toFixed(2)}</span>
                  </div>
                </div>
              )}
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default Orders;