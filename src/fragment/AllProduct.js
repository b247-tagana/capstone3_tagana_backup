import React, { Component } from 'react';
import Loading from '../components/Loading';
import ProductContext from '../context/ProductContext';

import Swal from 'sweetalert2';
import { Link } from 'react-router-dom';

import UpdateProduct from '../fragment/UpdateProduct'
import ModalUpdate from '../fragment/ModalUpdate'

import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';

class AllProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      cart: [],
      updateProduct: [],
      flag: "",
      category: '',
      image: '',
      name: '',
      description: '',
      quantity: '',
      price: '',
      checkFormFilled: false
    };
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  componentDidMount() {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then(res => res.json())
      .then(data => {
        this.setState({products: data})
        console.log(data)
      })
  }

  handleEditClick(_id) {
    this.setState({openModal: true})
    let update = this.state.products.find(product => {
      return product._id === _id
    })
    this.setState({updateProduct: update, flag: "update"})
    console.log(update)

  }

  handleArchiveClick(_id) {
    Swal.fire({
      title: 'Are you sure you want to archive this product?',
      iconColor: '#edae49',
      icon: 'warning',
      showCancelButton: true,
      reverseButtons: true,
      confirmButtonColor: '#edae49',
      confirmButtonText: 'Yes, archive it!',
      cancelButtonText: 'No, cancel!'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Product Archived!',
          'Your product has been archived.',
          'success'
        );
        const productId = _id;
        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json'
          },
        })
        .then(res => res.json())
        .then(data => {
          this.componentDidMount();
          console.log(data);
        })
        .catch(error => console.error(error));
        console.log('archive product with id:', _id);
      }
    })
  }

  handleActivateClick(_id) {
    Swal.fire({
      title: 'Are you sure you want to activate this product?',
      iconColor: '#38a169',
      icon: 'warning',
      showCancelButton: true,
      reverseButtons: true,
      confirmButtonColor: '#38a169',
      confirmButtonText: 'Yes, activate it!',
      cancelButtonText: 'No, cancel!'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Product Activated!',
          'Your product has been activated.',
          'success'
        );
        const productId = _id;
        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/activate`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json'
          },
        })
        .then(res => res.json())
        .then(data => {
          this.componentDidMount();
          console.log(data);
        })
        .catch(error => console.error(error));
        console.log('activate product with id:', _id);
      }
    })
  }

  handleDeleteClick(_id) {
    Swal.fire({
      title: 'Are you sure you want to delete this product?',
      text: 'You will not be able to recover this product after deletion!',
      iconColor: '#d1495b',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d1495b',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        const productId = _id;
        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/delete`, {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json'
          },
        })
        .then(res => res.json())
        .then(data => {
          this.componentDidMount();
          console.log(data);
          Swal.fire(
            'Deleted!',
            'The product has been deleted.',
            'success'
          )
        })
        .catch(error => console.error(error));
        console.log('delete product with id:', _id);
      }
    })
  }

  handleMenuHover(menuIndex) {
    if(this.state.selectedMenu !== menuIndex) {
      // setSelectedMenu(menuIndex);
      this.setState({
        selectedMenu: menuIndex
      });
    }
  }

  handleInputChange(event) {
    const { name, value } = event.target;
    this.setState(prevState => ({
      updateProduct: {
        ...prevState.updateProduct,
        [name]: value
      }
    }));
  }

  handleUpdateClick(_id) {
    Swal.fire({
      title: 'Are you sure you want to update this product?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      reverseButtons: true,
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, update it!',
      cancelButtonText: 'No, cancel'
    }).then((result) => {
      if (result.isConfirmed) {
        const updatedFields = {};
        Object.entries(this.state.updateProduct).forEach(([key, value]) => {
          if (value !== this.state.products.find(product => product._id === _id)[key]) {
            updatedFields[key] = value;
          }
        });

        // Perform the update only if there are modified fields
        if (Object.keys(updatedFields).length > 0) {
          fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/update`, {
            method: 'PUT',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(updatedFields)
          })
            .then(res => res.json())
            .then(data => {
              console.log(data);
              if (data === true) {
                Swal.fire({
                  icon: 'success',
                  title: 'Product updated successfully!',
                  showConfirmButton: false,
                  timer: 1500
                });
              } else {
                Swal.fire({
                  icon: 'error',
                  title: 'Failed to update product',
                  text: data.message || 'An unknown error occurred.',
                  showConfirmButton: false,
                  timer: 1500
                });
              }
              this.componentDidMount();
              this.setState({ openModal: false });
            })
            .catch(error => {
              console.error(error);
              Swal.fire({
                icon: 'error',
                title: 'Failed to update product',
                text: 'An error occurred while updating the product.',
                showConfirmButton: false,
                timer: 1500
              });
            });
          }
       }
    });
  }

  render() {
    const filteredProducts = this.state.products.filter((item) => {
      return (
        item.isActive === true ||
        item.isActive === false
      );
    }).sort((a, b) => {
      if (a.isActive === b.isActive) {
        return 0;
      } else if (a.isActive) {
        return -1;
      } else {
        return 1;
      }
    });

    return (
      <div className="p-5 mt-[12px]">
        <h1 className="text-3xl font-bold mb-[45px]">All Products</h1>
        <div className="border-b border-gray-300 mb-8"></div>
        <div className="bg-white p-10 rounded-md shadow-md mx-auto overflow-auto" style={{maxHeight: 'calc(100vh - 200px)'}}>
          <div className="table-wrapper overflow-x-auto">
            <table className="table-fixed w-full">
              <thead>
                <tr>
                  <th className="w-[90px] py-2 px-2 text-left">Name</th>
                  <th className="w-[40px] py-2 px-2 text-left">Image</th>
                  <th className="w-[200px] py-2 px-2 text-left">Description</th>
                  <th className="w-[70px] py-2 px-2 text-left text-center">Category</th>
                  <th className="w-[60px] py-2 px-2 text-left text-center">Quantity</th>
                  <th className="w-[60px] py-2 px-2 text-left text-center">Price</th>
                  <th className="w-[60px] py-2 px-2 text-left text-center">Status</th>
                  <th className="w-[60px] py-2 px-2 text-left"></th>
                </tr>
              </thead>
              <tbody>
                {filteredProducts.map((product) => (
                    <tr key={product._id} className="border-b">
                      <td className="py-2 px-2 font-semibold">{product.name}</td>
                      <td className="py-2 px-2">
                        <img className="w-20 object-cover shadow-md" src={product.image} alt={product.name} />
                      </td>
                      <td className="py-2 px-2">{product.description}</td>
                      <td className="py-2 px-4 text-center">{product.category}</td>
                      <td className="py-2 px-2 text-center">{product.quantity}</td>
                      <td className="py-2 px-2 text-center">{product.price}</td>
                      {product.isActive ?
                        <td className="py-2 px-2 text-center text-green-600 font-semibold">Active</td> :
                        <td className="py-2 px-2 text-center text-red-600 font-semibold">Inactive</td>
                      }
                    <td className="px-3 py-2">
                      {product.isActive ? (
                        <>
                            <button
                              className="bg-[#30638e] hover:bg-blue-600 text-white font-bold py-2 px-4 rounded mb-1 w-[110px]"
                              onClick={() => this.handleEditClick(product._id)}
                            >
                              Edit
                            </button>

                          <button
                            className="bg-[#edae49] hover:bg-yellow-400 text-white font-bold py-2 px-4 rounded mb-1 w-[110px]"
                            onClick={() => this.handleArchiveClick(product._id)}
                          >
                            Archive
                          </button>

                          <button
                            className="bg-[#d1495b] hover:bg-red-600 text-white font-bold py-2 px-4 rounded mb-1 w-[110px]"
                            onClick={() => this.handleDeleteClick(product._id)}
                          >
                            Delete
                          </button>
                        </>
                      ) : (
                        <>
                          <button
                            className="bg-[#30638e] hover:bg-blue-600 text-white font-bold py-2 px-4 rounded mb-1 w-[110px]"
                            onClick={() => this.handleEditClick(product._id)}
                          >
                            Edit
                          </button>

                          <button
                            className="bg-[#38a169] hover:bg-green-400 text-white font-bold py-2 px-2 rounded mb-1 w-[110px]"
                            onClick={() => this.handleActivateClick(product._id)}
                          >
                            Activate
                          </button>

                          <button
                            className="bg-[#d1495b] hover:bg-red-600 text-white font-bold py-2 px-4 rounded mb-1 w-[110px]"
                            onClick={() => this.handleDeleteClick(product._id)}
                          >
                            Delete
                          </button>
                        </>
                      )}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>  
        </div>
        <div>

          <Modal show={this.state.openModal}
            onHide={() => this.setState({ openModal: false })}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            >
            <Modal.Header closeButton>
              <Modal.Title className="text-center">Update</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group className="mb-3" controlId="category">
                  <Form.Label>Category</Form.Label>
                  <Form.Control
                    type="text"
                    name="category" // Update the name to "category"
                    defaultValue={this.state.updateProduct.category}
                    onChange={this.handleInputChange}
                    autoFocus
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="productName">
                  <Form.Label>Image URL</Form.Label>
                  <Form.Control
                    type="text"
                    name="image" // Update the name to "image"
                    defaultValue={this.state.updateProduct.image}
                    onChange={this.handleInputChange}                    
                    maxLength={20}
                    autoFocus
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                  <Form.Label>Name</Form.Label>
                  <Form.Control
                    type="text"
                    name="name" // Update the name to "name"
                    defaultValue={this.state.updateProduct.name}
                    onChange={this.handleInputChange}
                    autoFocus
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                  <Form.Label>Description</Form.Label>
                  <Form.Control
                    type="text"
                    name="description" // Update the name to "description"
                    defaultValue={this.state.updateProduct.description}
                    onChange={this.handleInputChange}
                    autoFocus
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                  <Form.Label>Quantity</Form.Label>
                  <Form.Control
                    type="text"
                    name="quantity" // Update the name to "quantity"
                    defaultValue={this.state.updateProduct.quantity}
                    onChange={this.handleInputChange}
                    autoFocus
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                  <Form.Label>Price</Form.Label>
                  <Form.Control
                    type="text"
                    name="price" // Update the name to "price"
                    defaultValue={this.state.updateProduct.price}
                    onChange={this.handleInputChange}
                    autoFocus
                  />
                </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary"
              onClick={() => this.setState({ openModal: false })}
              >
                Close
              </Button>
              <Button
                variant="primary"
                onClick={() => this.handleUpdateClick(this.state.updateProduct._id)}
              >
                Update
              </Button>

            </Modal.Footer>
          </Modal>

        </div>
      </div>
    );
  }
}

export default AllProduct;