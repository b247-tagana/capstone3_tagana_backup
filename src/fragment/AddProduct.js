import React, { Component } from 'react';

import Swal from 'sweetalert2';

class AddProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      category: '',
      image: '',
      name: '',
      description: '',
      quantity: '',
      price: '',
      formFilled: false
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleAddProduct = this.handleAddProduct.bind(this);
  }

  checkFormFilled() {
    const { category, image, name, description, quantity, price } = this.state;
    const formFilled = Boolean(category && image && name && description && quantity && price);
    this.setState({ formFilled });
  }

  handleInputChange(event) {
    const { name, value } = event.target;
    this.setState({ [name]: value }, () => {
      if (["category", "image", "name", "description", "quantity", "price"].includes(name)) {
        this.checkFormFilled();
      }
    });
  }


  handleAddProduct() {
    Swal.fire({
      title: 'Are you sure you want to add this product?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      reverseButtons: true,
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, add it!',
      cancelButtonText: 'No, cancel'
    }).then((result) => {
      if (result.isConfirmed) {
        fetch(`${process.env.REACT_APP_API_URL}/products/`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            category: this.state.category,
            image: this.state.image,
            name: this.state.name,
            description: this.state.description,
            quantity: this.state.quantity,
            price: this.state.price,
          })
        })
        .then(res => res.json())
        .then(data => {
          console.log(data)
          Swal.fire({
            icon: 'success',
            title: 'Product added successfully!',
            showConfirmButton: false,
            timer: 1500
          })
          // Clear the form
          this.setState({
            category: '',
            image: '',
            name: '',
            description: '',
            quantity: '',
            price: '',
            formFilled: false
          });
        })
        .catch(error => console.error(error));
      }
    })
  }

  render() {
  return (
   <div className="p-5 mt-[12px]">
        <h1 className="text-3xl font-bold mb-[45px]">Create Product</h1>
      <div className="border-b border-gray-300 mt-[36px]"></div>
      <div className="bg-white p-10 mt-10 h-[730px] w-[600px] rounded-md shadow-md mx-auto">

        {/* Category */}
        <div className="mb-2 mt-3">
          <label className="block text-gray-700 font-bold mb-1" htmlFor="category">
            Category
          </label>
          <input
            className="appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="category"
            name="category"
            type="text"
            // value={this.state.category}
            onChange={(e) => this.handleInputChange(e)}
            placeholder="Enter category"
          />
        </div>

        {/* Image */}
        <div className="mb-2">
          <label className="block text-gray-700 font-bold mb-1" htmlFor="image">
            Image URL
          </label>
          <input
            className="appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="image"
            name="image"
            type="text"
            // value={this.state.image}
            onChange={(e) => this.handleInputChange(e)}
            placeholder="Enter image URL"
          />
        </div>

        {/* Name */}
        <div className="mb-2">
          <label className="block text-gray-700 font-bold mb-1" htmlFor="name">
            Name
          </label>
          <input
            className="appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="name"
            name="name"
            type="text"
            // value={this.state.name}
            onChange={(e) => this.handleInputChange(e)}
            placeholder="Enter name"
          />
        </div>

        {/* Description */}
        <div className="mb-2">
          <label className="block text-gray-700 font-bold mb-1" htmlFor="description">
            Description
          </label>
          <textarea
            className="appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline h-32 resize-vertical resize-none"
            id="description"
            name="description"
            // value={this.state.description}
            onChange={(e) => this.handleInputChange(e)}
            placeholder="Enter description"
          />
        </div>

        {/*Quantity*/}
        <div className="mb-2">
          <label className="block text-gray-700 font-bold mb-1" htmlFor="quantity">
            Quantity
          </label>
          <input
            className="appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="quantity"
            name="quantity"
            type="text"
            // value={this.state.quantity}
            onChange={(e) => this.handleInputChange(e)}
            placeholder="Enter quantity"
          />
        </div>

        {/*Price*/}
        <div className="mb-2">
          <label className="block text-gray-700 font-bold mb-1" htmlFor="price">
            Price
          </label>
          <input
            className="appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="price"
            name="price"
            type="text"
            // value={this.state.price}
            onChange={(e) => this.handleInputChange(e)}
            placeholder="Enter price"
          />
        </div>

        <div className="flex justify-center items-center">
          <button
            className={`mt-4 w-full py-3 px-4 rounded font-bold text-white focus:outline-none focus:shadow-outline ${
              this.state.formFilled === true ? 'bg-blue-500 hover:bg-blue-700' : 'bg-red-400'
            }`}
            onClick={() => this.handleAddProduct()}
            disabled={!this.state.formFilled}
          >
            Create
          </button>
        </div> 
      </div>
    </div>
    );
  }
}

export default AddProduct;