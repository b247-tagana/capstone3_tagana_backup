import React, { Component } from 'react';
import Loading from '../components/Loading';
import ProductContext from '../context/ProductContext';

import Swal from 'sweetalert2';

class ActiveProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      cart: [],
    };
  }

  componentDidMount() {
    fetch(`${process.env.REACT_APP_API_URL}/products/active`)
      .then(res => res.json())
      .then(data => {
        this.setState({products: data})
        console.log(data)
      })
  }

  handleUpdateClick(_id) {
    console.log('update product with id:', _id);
  }

  handleArchiveClick(_id) {
    Swal.fire({
      title: 'Are you sure you want to archive this product?',
      iconColor: '#edae49',
      icon: 'warning',
      showCancelButton: true,
      reverseButtons: true,
      confirmButtonColor: '#edae49',
      confirmButtonText: 'Yes, archive it!',
      cancelButtonText: 'No, cancel!'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Product Archived!',
          'Your product has been archived.',
          'success'
        );
        const productId = _id;
        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json'
          },
        })
        .then(res => res.json())
        .then(data => {
          this.componentDidMount();
          console.log(data);
        })
        .catch(error => console.error(error));
        console.log('archive product with id:', _id);
      }
    })
  }

  handleActivateClick(_id) {
    Swal.fire({
      title: 'Are you sure you want to activate this product?',
      iconColor: '#38a169',
      icon: 'warning',
      showCancelButton: true,
      reverseButtons: true,
      confirmButtonColor: '#38a169',
      confirmButtonText: 'Yes, activate it!',
      cancelButtonText: 'No, cancel!'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Product Activated!',
          'Your product has been activated.',
          'success'
        );
        const productId = _id;
        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/activate`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json'
          },
        })
        .then(res => res.json())
        .then(data => {
          this.componentDidMount();
          console.log(data);
        })
        .catch(error => console.error(error));
        console.log('activate product with id:', _id);
      }
    })
  }


  handleDeleteClick(_id) {
    Swal.fire({
      title: 'Are you sure you want to delete this product?',
      text: 'You will not be able to recover this product after deletion!',
      iconColor: '#d1495b',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d1495b',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        const productId = _id;
        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/delete`, {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json'
          },
        })
        .then(res => res.json())
        .then(data => {
          this.componentDidMount();
          console.log(data);
          Swal.fire(
            'Deleted!',
            'The product has been deleted.',
            'success'
          )
        })
        .catch(error => console.error(error));
        console.log('delete product with id:', _id);
      }
    })
  }


  render() {
    const filteredProducts = this.state.products.filter((item) => {
      return (
        item.isActive === true ||
        item.isActive === false
      );
    }).sort((a, b) => {
      if (a.isActive === b.isActive) {
        return 0;
      } else if (a.isActive) {
        return -1;
      } else {
        return 1;
      }
    });

    return (
      <div className="p-5 mt-[12px]">
        <h1 className="text-3xl font-bold mb-[45px]">Active Products</h1>
        <div className="border-b border-gray-300 mb-8"></div>
        <div className="bg-white p-10 rounded-md shadow-md mx-auto overflow-auto" style={{maxHeight: 'calc(100vh - 200px)'}}>
          <div className="table-wrapper overflow-x-auto">
            <table className="table-fixed w-full">
              <thead>
                <tr>
                  <th className="w-[90px] py-2 px-2 text-left">Name</th>
                  <th className="w-[40px] py-2 px-2 text-left">Image</th>
                  <th className="w-[200px] py-2 px-2 text-left">Description</th>
                  <th className="w-[70px] py-2 px-2 text-left text-center">Category</th>
                  <th className="w-[60px] py-2 px-2 text-left text-center">Quantity</th>
                  <th className="w-[60px] py-2 px-2 text-left text-center">Price</th>
                  <th className="w-[60px] py-2 px-2 text-left text-center">Status</th>
                  <th className="w-[60px] py-2 px-2 text-left"></th>
                </tr>
              </thead>
              <tbody>
                {filteredProducts.map((product) => (
                    <tr key={product._id} className="border-b">
                      <td className="py-2 px-2 font-semibold">{product.name}</td>
                      <td className="py-2 px-2">
                        <img className="w-20 object-cover shadow-md" src={product.image} alt={product.name} />
                      </td>
                      <td className="py-2 px-2">{product.description}</td>
                      <td className="py-2 px-4 text-center">{product.category}</td>
                      <td className="py-2 px-2 text-center">{product.quantity}</td>
                      <td className="py-2 px-2 text-center">{product.price}</td>
                      {product.isActive ?
                        <td className="py-2 px-2 text-center text-green-600 font-semibold">Active</td> :
                        <td className="py-2 px-2 text-center">Inactive</td>
                      }
                    <td className="px-3 py-2">
                      {product.isActive ? (
                        <>
                          <button
                            className="bg-[#30638e] hover:bg-blue-600 text-white font-bold py-2 px-4 rounded mb-1 w-[110px]"
                            onClick={() => this.handleUpdateClick(product._id)}
                          >
                            Update
                          </button>

                          <button
                            className="bg-[#edae49] hover:bg-yellow-400 text-white font-bold py-2 px-4 rounded mb-1 w-[110px]"
                            onClick={() => this.handleArchiveClick(product._id)}
                          >
                            Archive
                          </button>

                          <button
                            className="bg-[#d1495b] hover:bg-red-600 text-white font-bold py-2 px-4 rounded mb-1 w-[110px]"
                            onClick={() => this.handleDeleteClick(product._id)}
                          >
                            Delete
                          </button>
                        </>
                      ) : (
                        <>
                          <button
                            className="bg-[#30638e] hover:bg-blue-600 text-white font-bold py-2 px-4 rounded mb-1 w-[110px]"
                            onClick={() => this.handleUpdateClick(product._id)}
                          >
                            Update
                          </button>

                          <button
                            className="bg-[#38a169] hover:bg-green-400 text-white font-bold py-2 px-2 rounded mb-1 w-[110px]"
                            onClick={() => this.handleActivateClick(product._id)}
                          >
                            Activate
                          </button>

                          <button
                            className="bg-[#d1495b] hover:bg-red-600 text-white font-bold py-2 px-4 rounded mb-1 w-[110px]"
                            onClick={() => this.handleDeleteClick(product._id)}
                          >
                            Delete
                          </button>
                        </>
                      )}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default ActiveProduct;
