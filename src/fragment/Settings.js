import React, { Component } from 'react';

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notifications: true,
      darkMode: false,
      autoPlay: true,
      fontSize: 'medium',
      fontTypeface: 'Arial',
      themeColor: '#3498db',
      emailNotifications: true,
      pushNotifications: true,
      smsNotifications: false,
      inAppNotifications: true,
    };
  }

  handleToggleNotifications = () => {
    this.setState(prevState => ({
      notifications: !prevState.notifications,
    }));
  };

  handleToggleDarkMode = () => {
    this.setState(prevState => ({
      darkMode: !prevState.darkMode,
    }));
  };

  handleToggleEmailNotifications = () => {
    this.setState(prevState => ({
      emailNotifications: !prevState.emailNotifications,
    }));
  };

  handleTogglePushNotifications = () => {
    this.setState(prevState => ({
      pushNotifications: !prevState.pushNotifications,
    }));
  };

  handleToggleSmsNotifications = () => {
    this.setState(prevState => ({
      smsNotifications: !prevState.smsNotifications,
    }));
  };

  handleFontSizeChange = event => {
    this.setState({ fontSize: event.target.value });
  };

  handleFontTypefaceChange = event => {
    this.setState({ fontTypeface: event.target.value });
  };

  handleThemeColorChange = event => {
    this.setState({ themeColor: event.target.value });
  };

  render() {
    return (
      <div className="p-5 mt-[12px]">
        <h1 className="text-3xl font-bold mb-[45px]">Settings</h1>
        <div className="border-b border-gray-300 mb-8"></div>
        <div className="bg-white p-10 rounded-md shadow-md h-[750px]">
          <div className="flex items-center justify-between mb-4 border-b">
            <p className="font-bold text-lg">Appearance</p>
          </div>
          {/*Dark Mode*/}
          <div className="flex items-center justify-between mb-2">
            <p className="font-medium">Dark mode</p>
            <div className="flex items-center">
              <button
                className={`${
                  this.state.darkMode ? 'bg-green-500' : 'bg-gray-300'
                } w-12 h-6 rounded-full transition-colors duration-300 flex justify-center`}
                onClick={this.handleToggleDarkMode}
              >
                <span
                  className={`${
                    this.state.darkMode ? 'translate-x-[11px]' : 'translate-x-[-11px]'
                  } inline-block w-4 h-4 bg-white rounded-full shadow-md transform transition-transform duration-300 mt-[3px]`}
                ></span>
              </button>
            </div>
          </div>
          {/*Font Size*/}
          <div className="flex items-center justify-between mb-2">
            <p className="font-medium">Font size</p>
            <select
              className="py-2 px-3 rounded-md border-gray-400 border focus:outline-none"
              value={this.state.fontSize}
              onChange={this.handleFontSizeChange}>
              <option value="small">Small</option>
              <option value="medium">Medium</option>
              <option value="large">Large</option>
            </select>
          </div>
          {/*Font Typeface*/}
          <div className="flex flex-col mb-2">
            <label htmlFor="font-typeface" className="mb-1 font-medium">
              Font Typeface
            </label>
            <select
              id="font-typeface"
              name="font-typeface"
              className="border rounded-md py-2 px-3 text-gray-500"
              onChange={this.handleFontTypefaceChange}
              value={this.state.fontTypeface}
            >
              <option value="sans-serif">Sans-serif</option>
              <option value="serif">Serif</option>
              <option value="monospace">Monospace</option>
            </select>
          </div>
          {/*Theme Color*/}
          <div className="flex flex-col mb-5">
            <label htmlFor="theme-color" className="mb-1 font-medium">
              Theme Color
            </label>
            <select
              id="theme-color"
              name="theme-color"
              className="border rounded-md py-2 px-3 text-gray-500"
              onChange={this.handleThemeColorChange}
              value={this.state.themeColor}
            >
              <option value="blue">Blue</option>
              <option value="green">Green</option>
              <option value="red">Red</option>
            </select>
          </div>
          <div className="flex items-center justify-between mb-4 border-b">
            <p className="font-bold text-lg">Notifications</p>
          </div>
          {/*Email Notif*/}
          <div className="flex items-center justify-between mb-2">
            <p className="font-medium">Email notifications</p>
            <div className="flex items-center">
              <button
                className={`${
                  this.state.darkMode ? 'bg-green-500' : 'bg-gray-300'
                } w-12 h-6 rounded-full transition-colors duration-300 flex justify-center`}
                onClick={this.handleToggleEmailNotifications}
              >
                <span
                  className={`${
                    this.state.darkMode ? 'translate-x-[11px]' : 'translate-x-[-11px]'
                  } inline-block w-4 h-4 bg-white rounded-full shadow-md transform transition-transform duration-300 mt-[3px]`}
                ></span>
              </button>
            </div>
          </div>
          {/*Push notif*/}
          <div className="flex items-center justify-between mb-2">
            <p className="font-medium">Push notifications</p>
            <div className="flex items-center">
              <button
                className={`${
                  this.state.darkMode ? 'bg-green-500' : 'bg-gray-300'
                } w-12 h-6 rounded-full transition-colors duration-300 flex justify-center`}
                onClick={this.handleTogglePushNotifications}
              >
                <span
                  className={`${
                    this.state.darkMode ? 'translate-x-[11px]' : 'translate-x-[-11px]'
                  } inline-block w-4 h-4 bg-white rounded-full shadow-md transform transition-transform duration-300 mt-[3px]`}
                ></span>
              </button>
            </div>
          </div>
          {/*Sms notif*/}
          <div className="flex items-center justify-between mb-2">
            <p className="font-medium">Sms notifications</p>
            <div className="flex items-center">
              <button
                className={`${
                  this.state.darkMode ? 'bg-green-500' : 'bg-gray-300'
                } w-12 h-6 rounded-full transition-colors duration-300 flex justify-center`}
                onClick={this.handleToggleSmsNotifications}
              >
                <span
                  className={`${
                    this.state.darkMode ? 'translate-x-[11px]' : 'translate-x-[-11px]'
                  } inline-block w-4 h-4 bg-white rounded-full shadow-md transform transition-transform duration-300 mt-[3px]`}
                ></span>
              </button>
            </div>
          </div>
          {/*Allow sound*/}
          <div className="flex items-center justify-between mb-2">
            <p className="font-medium">Allow sound</p>
            <div className="flex items-center">
              <button
                className={`${
                  this.state.darkMode ? 'bg-green-500' : 'bg-gray-300'
                } w-12 h-6 rounded-full transition-colors duration-300 flex justify-center`}
                onClick={this.handleToggleSmsNotifications}
              >
                <span
                  className={`${
                    this.state.darkMode ? 'translate-x-[11px]' : 'translate-x-[-11px]'
                  } inline-block w-4 h-4 bg-white rounded-full shadow-md transform transition-transform duration-300 mt-[3px]`}
                ></span>
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Settings;